import { ApolloServer } from 'apollo-server';
import { resolvers, typeDefs } from './graph';

const server = new ApolloServer({
  typeDefs,
  resolvers,
});

server.listen({ port: process.env.PORT ?? 4000 })
  .then((url) => console.log(`🚀 Server ready at ${url.url}`))
  .catch(console.error);
