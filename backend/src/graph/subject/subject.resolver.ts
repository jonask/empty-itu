import { MutationCreateSubjectArgs } from '../../generated/graphql';
import * as subjectController from './subject.controller';

const Query = {
  getByEmail: (_, args) => (
    subjectController.getByEmail(args.email)
  )
}

const Mutation = {
  create: (_, args: MutationCreateSubjectArgs) => (
    subjectController.create(args.email)
  ),
}

const definitions = {
  Query,
  Mutation,
};