import { prisma } from '../../client';

export async function create(email: string) {
  return prisma.subject.create({
    data: {
      email,
    },
  });
}

export async function getByEmail(email: string) {
  return prisma.subject.findUniqueOrThrow({
    where: {
      email,
    },
  });
}