import path from 'path';
import { loadFilesSync } from '@graphql-tools/load-files';

const typesArray = loadFilesSync(path.join(__dirname, './**/*.graphql'));

export default typesArray;
