import resolvers from './mergeResolvers';
import typeDefs from './mergeSchema';

export {
  resolvers,
  typeDefs,
};
